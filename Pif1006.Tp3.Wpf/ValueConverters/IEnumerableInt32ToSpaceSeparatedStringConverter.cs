﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Pif1006.Tp3.Wpf.ValueConverters
{
    [ValueConversion(typeof(IEnumerable<int>), typeof(string))]
    public class IEnumerableInt32ToSpaceSeparatedStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IEnumerable<int> enumerableValue)
            {
                return string.Join(" ", enumerableValue.Select(v => v.ToString(CultureInfo.InvariantCulture)));
            }

            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string stringValue)
            {
                var substrings = stringValue.Split(" ");
                var array = new int[substrings.Length];
                var i = 0;

                foreach (var substring in substrings)
                {
                    if (int.TryParse(substring, out var intValue))
                    {
                        array[i++] = intValue;
                    }
                    else
                    {
                        return DependencyProperty.UnsetValue;
                    }
                }

                return array;
            }

            return DependencyProperty.UnsetValue;
        }
    }
}
