﻿using Pif1006.Tp3.Core.Interfaces;

namespace Pif1006.Tp3.Core
{
    public class CbcEncrypter : IEncrypter
    {
        private readonly byte _iv;
        private readonly IEncryptionFunction _function;

        public CbcEncrypter(byte iv)
            : this(iv, null)
        {
        }

        public CbcEncrypter(byte iv, IEncryptionFunction function)
        {
            _iv = iv;
            _function = function ?? NullEncryptionFunction.Instance;
        }

        public byte[] Encrypt(byte[] value)
        {
            if (value == null)
            {
                return null;
            }

            var length = value.Length;
            var encrypted = new byte[length];

            if (encrypted.Length > 0)
            {
                encrypted[0] = _function.Encrypt((byte)(value[0] ^ _iv));
            }

            for (var i = 1; i < length; i++)
            {
                encrypted[i] = _function.Encrypt((byte)(value[i] ^ encrypted[i - 1]));
            }

            return encrypted;
        }
    }
}
