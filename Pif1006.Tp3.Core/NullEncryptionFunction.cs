﻿using Pif1006.Tp3.Core.Interfaces;

namespace Pif1006.Tp3.Core
{
    public class NullEncryptionFunction : IEncryptionFunction, IDecryptionFunction
    {
        public static NullEncryptionFunction Instance { get; } = new();

        private NullEncryptionFunction()
        {
        }

        public byte Encrypt(byte value)
        {
            return value;
        }

        public byte Decrypt(byte value)
        {
            return value;
        }
    }
}
