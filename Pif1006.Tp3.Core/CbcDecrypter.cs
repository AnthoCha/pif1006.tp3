﻿using Pif1006.Tp3.Core.Interfaces;

namespace Pif1006.Tp3.Core
{
    public class CbcDecrypter : IDecrypter
    {
        private readonly byte _iv;
        private readonly IDecryptionFunction _function;

        public CbcDecrypter(byte iv)
            : this(iv, null)
        {
        }

        public CbcDecrypter(byte iv, IDecryptionFunction function)
        {
            _iv = iv;
            _function = function ?? NullEncryptionFunction.Instance;
        }

        public byte[] Decrypt(byte[] value)
        {
            if (value == null)
            {
                return null;
            }

            var length = value.Length;
            var decrypted = new byte[length];

            if (decrypted.Length > 0)
            {
                decrypted[0] = (byte)(_function.Decrypt(value[0]) ^ _iv);
            }

            for (var i = 1; i < length; i++)
            {
                decrypted[i] = (byte)(_function.Decrypt(value[i]) ^ value[i - 1]);
            }

            return decrypted;
        }
    }
}
