﻿using Pif1006.Tp3.Core.Interfaces;
using Pif1006.Tp3.Resources;
using System;
using System.Linq;

namespace Pif1006.Tp3.Core
{
    public class TranspositionEncrypter : IEncrypter, IDecrypter
    {
        private readonly int[] _colMap;
        private readonly int _keyLength;

        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public TranspositionEncrypter(int[] key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            _keyLength = key.Length;

            // key must be from 1 to n
            if (!key.OrderBy(k => k).SequenceEqual(Enumerable.Range(1, _keyLength)))
            {
                throw new ArgumentOutOfRangeException(nameof(key), key, ErrorMessages.TranspositionKeyOutOfRange);
            }

            _colMap = new int[_keyLength];

            for (var i = 0; i < _keyLength; i++)
            {
                // map key column index to clear text column index
                _colMap[key[i] - 1] = i;
            }
        }

        public byte[] Encrypt(byte[] value)
        {
            if (value == null)
            {
                return null;
            }

            var length = value.Length;
            var encrypted = new byte[length];

            var i = 0;
            for (var j = 0; j < _keyLength && j < length; j++)
            {
                var col = _colMap[j];
                for (var row = 0; (row * _keyLength) + col < length; row++)
                {
                    encrypted[i++] = value[(row * _keyLength) + col];
                }
            }

            return encrypted;
        }

        public byte[] Decrypt(byte[] value)
        {
            if (value == null)
            {
                return null;
            }

            var length = value.Length;
            var decrypted = new byte[length];

            var i = 0;
            for (var j = 0; j < _keyLength && j < length; j++)
            {
                var col = _colMap[j];
                for (var row = 0; (row * _keyLength) + col < length; row++)
                {
                    decrypted[(row * _keyLength) + col] = value[i++];
                }
            }

            return decrypted;
        }
    }
}
