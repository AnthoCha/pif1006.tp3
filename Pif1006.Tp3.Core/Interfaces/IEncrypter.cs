﻿namespace Pif1006.Tp3.Core.Interfaces
{
    public interface IEncrypter
    {
        byte[] Encrypt(byte[] value);
    }
}
