﻿namespace Pif1006.Tp3.Core.Interfaces
{
    public interface IEncryptionFunction
    {
        byte Encrypt(byte value);
    }
}
