﻿namespace Pif1006.Tp3.Core.Interfaces
{
    public interface IDecryptionFunction
    {
        byte Decrypt(byte value);
    }
}
