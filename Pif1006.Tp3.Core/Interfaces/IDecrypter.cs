﻿namespace Pif1006.Tp3.Core.Interfaces
{
    public interface IDecrypter
    {
        byte[] Decrypt(byte[] value);
    }
}
