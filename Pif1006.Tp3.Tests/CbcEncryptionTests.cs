using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pif1006.Tp3.Core;
using System.Linq;
using System.Text;

namespace Pif1006.Tp3.Tests
{
    [TestClass]
    public class CbcEncryptionTests
    {
        [TestMethod]
        public void TestEncryptionDecryption()
        {
            // Arrange
            var encrypter = new CbcEncrypter(42);
            var decrypter = new CbcDecrypter(42);
            var value = Encoding.ASCII.GetBytes("ce cours de mathematiques est tres interessant");

            // Act
            var encrypted = encrypter.Encrypt(value);
            var decrypted = decrypter.Decrypt(encrypted);

            // Assert
            Assert.IsTrue(value.SequenceEqual(decrypted));
        }
    }
}
