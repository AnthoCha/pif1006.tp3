using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pif1006.Tp3.Core;
using System.Linq;
using System.Text;

namespace Pif1006.Tp3.Tests
{
    [TestClass]
    public class TranspositionEncryptionTests
    {
        [TestMethod]
        public void TestEncryptionDecryption()
        {
            // Arrange
            var encrypter = new TranspositionEncrypter(new int[] { 7, 1, 4, 5, 2, 3, 8, 6 });
            var value = Encoding.ASCII.GetBytes("ce cours de mathematiques est tres interessant");

            // Act
            var encrypted = encrypter.Encrypt(value);
            var decrypted = encrypter.Decrypt(encrypted);

            // Assert
            Assert.IsTrue(value.SequenceEqual(decrypted));
        }
    }
}
