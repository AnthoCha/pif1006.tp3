﻿using Pif1006.Tp3.Models;
using System;
using System.Linq;
using System.Text;
using System.Windows;

namespace Pif1006.Tp3.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainWindowModel _model = new();
        private readonly Encoding _encoding = Encoding.ASCII;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = _model;
        }

        private void OnEncrypt(object sender, RoutedEventArgs e)
        {
            try
            {
                var encrypter = new Encrypter(_model.EncryptionIv, _model.EncryptionTranspositionKey.ToArray());
                var value = _encoding.GetBytes(_model.EncryptionClearText);
                var encrypted = encrypter.Encrypt(value);
                _model.EncryptionBase64EncryptedText = Convert.ToBase64String(encrypted);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OnDecrypt(object sender, RoutedEventArgs e)
        {
            try
            {
                var decrypter = new Decrypter(_model.DecryptionIv, _model.DecryptionTranspositionKey.ToArray());
                var value = Convert.FromBase64String(_model.DecryptionBase64EncryptedText);
                var decrypted = decrypter.Decrypt(value);
                _model.DecryptionClearText = _encoding.GetString(decrypted);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
