﻿using Pif1006.Tp3.Core;
using Pif1006.Tp3.Core.Interfaces;

namespace Pif1006.Tp3
{
    public class Decrypter : IDecrypter
    {
        private readonly CbcDecrypter _cbcDecrypter;
        private readonly TranspositionEncrypter _transpositionEncrypter;

        public Decrypter(byte iv, int[] transpositionKey)
        {
            _cbcDecrypter = new CbcDecrypter(iv);
            _transpositionEncrypter = new TranspositionEncrypter(transpositionKey);
        }

        public byte[] Decrypt(byte[] value)
        {
            return _transpositionEncrypter.Decrypt(_cbcDecrypter.Decrypt(value));
        }
    }
}
