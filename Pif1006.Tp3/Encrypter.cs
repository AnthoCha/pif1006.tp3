﻿using Pif1006.Tp3.Core;
using Pif1006.Tp3.Core.Interfaces;

namespace Pif1006.Tp3
{
    public class Encrypter : IEncrypter
    {
        private readonly CbcEncrypter _cbcEncrypter;
        private readonly TranspositionEncrypter _transpositionEncrypter;

        public Encrypter(byte iv, int[] transpositionKey)
        {
            _cbcEncrypter = new CbcEncrypter(iv);
            _transpositionEncrypter = new TranspositionEncrypter(transpositionKey);
        }

        public byte[] Encrypt(byte[] value)
        {
            return _cbcEncrypter.Encrypt(_transpositionEncrypter.Encrypt(value));
        }
    }
}
