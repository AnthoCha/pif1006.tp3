﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Pif1006.Tp3.Models
{
    public class MainWindowModel : DependencyObject
    {
        public byte EncryptionIv
        {
            get
            {
                return (byte)GetValue(EncryptionIvProperty);
            }
            set
            {
                SetValue(EncryptionIvProperty, value);
            }
        }

        public IEnumerable<int> EncryptionTranspositionKey
        {
            get
            {
                return (IEnumerable<int>)GetValue(EncryptionTranspositionKeyProperty);
            }
            set
            {
                SetValue(EncryptionTranspositionKeyProperty, value);
            }
        }

        public string EncryptionClearText
        {
            get
            {
                return (string)GetValue(EncryptionClearTextProperty);
            }
            set
            {
                SetValue(EncryptionClearTextProperty, value);
            }
        }

        public string EncryptionBase64EncryptedText
        {
            get
            {
                return (string)GetValue(EncryptionBase64EncryptedTextProperty);
            }
            set
            {
                SetValue(EncryptionBase64EncryptedTextProperty, value);
            }
        }

        public byte DecryptionIv
        {
            get
            {
                return (byte)GetValue(DecryptionIvProperty);
            }
            set
            {
                SetValue(DecryptionIvProperty, value);
            }
        }

        public IEnumerable<int> DecryptionTranspositionKey
        {
            get
            {
                return (IEnumerable<int>)GetValue(DecryptionTranspositionKeyProperty);
            }
            set
            {
                SetValue(DecryptionTranspositionKeyProperty, value);
            }
        }

        public string DecryptionBase64EncryptedText
        {
            get
            {
                return (string)GetValue(DecryptionBase64EncryptedTextProperty);
            }
            set
            {
                SetValue(DecryptionBase64EncryptedTextProperty, value);
            }
        }

        public string DecryptionClearText
        {
            get
            {
                return (string)GetValue(DecryptionClearTextProperty);
            }
            set
            {
                SetValue(DecryptionClearTextProperty, value);
            }
        }

        public static readonly DependencyProperty EncryptionIvProperty = DependencyProperty.Register(nameof(EncryptionIv), typeof(byte), typeof(MainWindowModel), new PropertyMetadata((byte)0));
        public static readonly DependencyProperty EncryptionTranspositionKeyProperty = DependencyProperty.Register(nameof(EncryptionTranspositionKey), typeof(IEnumerable<int>), typeof(MainWindowModel), new PropertyMetadata(Enumerable.Empty<int>()));
        public static readonly DependencyProperty EncryptionClearTextProperty = DependencyProperty.Register(nameof(EncryptionClearText), typeof(string), typeof(MainWindowModel), new PropertyMetadata(string.Empty));
        public static readonly DependencyProperty EncryptionBase64EncryptedTextProperty = DependencyProperty.Register(nameof(EncryptionBase64EncryptedText), typeof(string), typeof(MainWindowModel), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty DecryptionIvProperty = DependencyProperty.Register(nameof(DecryptionIv), typeof(byte), typeof(MainWindowModel), new PropertyMetadata((byte)0));
        public static readonly DependencyProperty DecryptionTranspositionKeyProperty = DependencyProperty.Register(nameof(DecryptionTranspositionKey), typeof(IEnumerable<int>), typeof(MainWindowModel), new PropertyMetadata(Enumerable.Empty<int>()));
        public static readonly DependencyProperty DecryptionBase64EncryptedTextProperty = DependencyProperty.Register(nameof(DecryptionBase64EncryptedText), typeof(string), typeof(MainWindowModel), new PropertyMetadata(string.Empty));
        public static readonly DependencyProperty DecryptionClearTextProperty = DependencyProperty.Register(nameof(DecryptionClearText), typeof(string), typeof(MainWindowModel), new PropertyMetadata(string.Empty));
    }
}
